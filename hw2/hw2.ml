(*
 *  CS 152 Programming Languages
 *
 *  Homework 2 Question 4
 *
 *
 *
 *  Instructions
 *
 *  Please fill in the placeholder functions per the written assignment
 *  Please do not change the signature of the functions
 *  Make sure that this file loads without any errors in a
 *     clean OCaml shell
 *
 *)



(*************************************************************

   Type for lambda terms

 ************************************************************)



type lterm = 
    LId of string
  | LLam of string * lterm
  | LApp of lterm * lterm



(*************************************************************

   Module implementing some helper functions
   
   LambdaParser.parse : string -> lterm
   LambdaParser.pp : lterm -> string

 ************************************************************)

module LambdaParser = struct

  let lexer = Genlex.make_lexer ["(";")";".";"/"]

  let lex s = 
    let str = lexer (Stream.of_string s)  in
    let rec loop () = 
      match (Stream.peek str) with
      | None -> []
      | Some _ -> let elt = Stream.next str in elt::(loop())  in
    loop ()

  let expect elt cs = 
    match cs with
    | f::cs when f = elt -> Some cs
    | _ -> None

  let expect_ident cs = 
    match cs with
    | (Genlex.Ident id)::cs -> Some (id,cs)
    | _ -> None

  let rec parse_term cs = 
    match parse_ident_terms cs with
    | Some x -> Some x
    | None -> 
	(match parse_lambda cs with
	|	Some x -> Some x
	|	None ->
	    (match parse_group_terms cs with
	    | Some x -> Some x
	    | None -> 
		(match parse_ident cs with
		|	Some x -> Some x
		|	None -> 
		    (match parse_group cs with
		    | Some x -> Some x
		    | None -> None))))

  and parse_ident_term cs = 
    match parse_ident cs with
    | None -> None
    | Some (term1,cs) -> 
	(match parse_term cs with
	|	None -> None
	|	Some (term2,cs) -> Some (LApp(term1,term2),cs))

  and parse_ident_terms cs =    (* ident term+ *)
    match parse_ident cs with
    | None -> None
    | Some (term1,cs) -> 
	(match parse_terms cs with
	|	None -> None
	|	Some (f,cs) -> Some (f term1,cs))

  and parse_group_terms cs =    (* group term+ *)
    match parse_group cs with
    | None -> None
    | Some (term1,cs) ->
	(match parse_terms cs with
	|	None -> None
	|	Some (f,cs) -> Some (f term1, cs))

  and parse_terms cs = 
    match parse_ident cs with
    | Some (term1,cs) -> 
	(match parse_terms cs with
	|	None -> Some ((fun t -> LApp(t,term1)),cs)
	|	Some (f,cs) -> Some ((fun t -> f (LApp (t,term1))),cs))
    | None-> 
	(match parse_group cs with
	|	Some (term1,cs) -> 
	    (match parse_terms cs with
	    | None -> Some ((fun t -> LApp(t,term1)),cs)
	    | Some (f,cs) -> Some ((fun t -> f (LApp (t,term1))),cs))
	|	None -> None)
	  

  and parse_ident cs =
    match expect_ident cs with
    | None -> None
    | Some (id,cs) -> Some (LId id,cs)

  and parse_lambda cs = 
    match expect (Genlex.Kwd "/") cs with
    | None -> None
    | Some cs -> 
	(match expect_ident cs with
	|	None -> None
	|	Some (id,cs) -> 
	    (match expect (Genlex.Kwd ".") cs with
	    | None -> None
	    | Some cs -> 
		(match parse_term cs with
		|	None -> None
		|	Some (term,cs) -> Some (LLam (id,term),cs))))

  and parse_group_term cs =
    match parse_group cs with
    | None -> None
    | Some (term1,cs) ->
	(match parse_term cs with
	|	None -> None
	|	Some (term2,cs) -> Some (LApp (term1,term2),cs))

  and parse_group cs =
    match expect (Genlex.Kwd "(") cs with
    | None -> None
    | Some cs ->
	(match parse_term cs with
	|	None -> None
	|	Some (term,cs) ->
	    (match expect (Genlex.Kwd ")") cs with
	    | None -> None
	    | Some cs -> Some (term,cs)))

  let parse str = 
    match parse_term (lex str) with
    | Some (term,[]) -> term
    | _ -> failwith ("Cannot parse "^str)

  let rec pp term = 
    match term with
    | LId x -> x
    | LLam (x,t) -> "/"^x^"."^(pp t)
    | LApp (t1,t2) -> 
	let t1' = (match t1 with
	| LLam _ -> "("^(pp t1)^")"
	| _ -> pp t1)  in
	let t2' = (match t2 with
	| LApp _ -> "("^(pp t2)^")"
	| LLam _ -> "("^(pp t2)^")"
	| _ -> pp t2)  in
	t1'^" "^t2'
end
    


(* PART A *)

(* Helper function to find free variables *)
let rec fv_helper (m:lterm) (non_free:string list) :string list =
match m with 
| LId s -> if List.exists (fun x -> x = s) non_free then [] else [s]
| LLam (s, n) -> fv_helper n (s::non_free)
| LApp (n1, n2) -> fv_helper n1 non_free  @ fv_helper n2 non_free

(* Find free variables *)
let rec fv (m:lterm) : string list = fv_helper m []


let gen_name (m:string list) : string = String.concat "_" m

let rec substitute (m:lterm) (s:string) (n:lterm):lterm =
match m with 
| LId ss ->  if ss = s then n else LId ss
| LLam (ss, nn) -> (
  if ss = s then LLam (ss, nn) else (
    let fv_e = fv n in 
    let fv_e' = fv nn in 
    let z = gen_name (s::fv_e @ fv_e') in 
    if List.exists (fun x -> x = ss) fv_e then (
      LLam (z, substitute (substitute nn ss (LId z)) s n)
    ) else (
      LLam (ss, substitute nn s n)
    )
  )

)
| LApp (n1, n2) -> LApp (substitute n1 s n, substitute n2 s n)



(* PART B *)

let rec reduce (t:lterm):lterm option =
match t with 
| LId s -> None
| LLam (s, n) -> (
  match reduce n with 
  | Some nn -> Some (LLam (s, nn))
  | None -> None
)
| LApp (n1, n2) -> (
  match n1 with 
  | LLam (s, e1) -> Some (substitute e1 s n2)
  | _ -> (
    match (reduce n1) with 
    | Some n11 -> Some (LApp (n11, n2))
    | None -> (
      match (reduce n2) with 
      | Some n22 -> Some (LApp (n1, n22))
      | None -> None
    )
  )
)



(* PART C *)

let rec normal_form (t:lterm):lterm =
match t with 
| LId s -> LId s
| LLam (s, n) -> (
  LLam (s, normal_form n)
)
| LApp (n1, n2) -> 
(
  match n1 with 
  | LLam (s, e1) -> normal_form (substitute e1 s n2)
  | _ -> ( 
    match reduce n1 with 
        | Some res -> normal_form (LApp (res, n2))
        | None -> (
    match reduce n2 with 
        | Some res -> normal_form (LApp (n1, res))
        | None -> LApp(n1, n2)
  )
  )
)


let eval (input:string):string =
  let term = LambdaParser.parse input in
  LambdaParser.pp (normal_form term)



(* PART D *)

type abbs = (string * lterm) list

(* sample encodings *)

let encs = let p = LambdaParser.parse in
           [ ("true", p "/x./y.x");
	     ("false", p "/x./y.y");
	     ("if", p "/c./x./y.c x y");
	     ("and", p "/b./c.b c false");
	     ("or", p "/b./c.b true c");
	     ("not", p "/b.b false true");
	     ("_0", p "/f./x.x");
	     ("_1", p "/f./x.(f x)");
	     ("_2", p "/f./x.(f (f x))");
	     ("_3", p "/f./x.(f (f (f x)))");
	     ("_4", p "/f./x.(f (f (f (f x))))");
	     ("_5", p "/f./x.(f (f (f (f (f x )))))");
	     ("succ", p "/n./f./x.f (n f x)");
	     ("plus", p "/m./n./f./x.(m f) (n f x)");
	     ("times", p "/m./n./f./x.m (n f) x");
	     ("iszero", p "/n.n (/x.false) true");
	     ("pred", p "/n./f./x.n (/g.(/h.h (g f))) (/u.x) (/u.u)");
	     ("Y", p "/f.(/x.f (x x)) (/x.f (x x))");
	     ("fact", p "Y (/fact./n.(iszero n) _1 (times n (fact (pred n))))") ]


let rec transform (abbs:abbs) (t:lterm) (non_free: string list):lterm = 
  match t with
  | LId s -> 
  ( if List.exists (fun x -> x = s) non_free then LId s else
  (match List.find_opt (fun (x, _) -> x = s) abbs with
              | Some (_, t) -> transform abbs t non_free
              | None -> LId s
              )
  )
  | LLam (s, n) -> LLam (s, transform abbs n (s::non_free))
  | LApp (n1, n2) -> LApp (transform abbs n1 non_free, transform abbs n2 non_free)


let normal_form_abbs (abbs:abbs) (t:lterm):lterm =
  let s = (LambdaParser.pp (transform abbs t [])) in 
  normal_form (transform abbs t [])

      
let eval_abbs (abbs:abbs) (input:string):string =
  let term = LambdaParser.parse input in
  LambdaParser.pp (normal_form_abbs abbs term)
