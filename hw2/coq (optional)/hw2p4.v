Require Import String.

Inductive lterm :=
| LId : string -> lterm
| LLam : string -> lterm -> lterm
| LApp : lterm -> lterm -> lterm.

(* substitute t2 for x in t1 *)
Fixpoint substitute (t1 : lterm) (x : string) (t2 : lterm) : lterm := t1 (* TODO *).

Inductive lstep : lterm -> lterm -> Prop :=
| LSId  : forall x,
            lstep (LId x) (LId x)
| LSLam : forall x e v,
            lstep e v ->
              lstep (LLam x e) (LLam x v)
| LSApp : forall x e1 e2 v1 v2 v,
            lstep e1 v1 ->
            lstep e2 v2 ->
            lstep (substitute e2 x e1) v ->
              lstep (LApp (LLam x e1) e2) v.

Fixpoint reduce (t : lterm) : option lterm := None (* TODO *).

Fixpoint normal_form (fuel : nat) (t : lterm) : option lterm := None (* TODO *).

(* CHALLENGE *)
Theorem interpreter_correct :
    forall e v, lstep e v <-> exists f, normal_form f e = Some v.
Proof.
Admitted.

Definition abbs := list (string * lterm).

Definition normal_form_abbs (fuel : nat) (a : abbs) (t : lterm) : option lterm := None (* TODO *).