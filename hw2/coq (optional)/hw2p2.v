Require Import Coq.omega.Omega.

(* We'll talk about monads later! For now I'm just using it for notational convenience. *)

Definition bind {A B : Type} : option A -> (A -> option B) -> option B :=
  fun x f => match x with None => None | Some a => f a end.
Notation "'do' X <- A ; B" := (bind A (fun X => B)) (at level 200, X ident, A at level 100, B at level 200).

Definition makeFib (f : nat -> option nat) (n : nat) : option nat :=
  match n with
  | 0 => Some 0
  | S n' => match n' with
            | 0 => Some 1
            | S n'' => do n1 <- f n'';
                       do n2 <- f n';
                       Some (n1 + n2)
            end
  end.

Fixpoint makeFibI (n : nat) : nat -> option nat :=
  match n with
  | 0 => fun _ => None
  | S n' => makeFib (makeFibI n')
  end.

(* ----- PROBLEM 2 (a) -----

   Expand makeFibI 2 and makeFibI 3. These functions will be
   pretty unreadable, so you might prove them equivaelnt to
   something nicer-looking. *)

Eval compute in makeFibI 2.
Eval compute in makeFibI 3.

(* For example... *)
Definition makeFibI1 (n : nat) : option nat :=
  match n with
  | 0 => Some 0
  | 1 => Some 1
  | _ => None
  end.

Definition makeFibI2 (n : nat) : option nat := None (* TODO *).
Definition makeFibI3 (n : nat) : option nat := None (* TODO *).

Example mf2_equiv : forall n, makeFibI2 n = makeFibI 2 n.
Proof.
(* TODO *)
Admitted.

Example mf3_equiv : forall n, makeFibI3 n = makeFibI 3 n.
Proof.
(* TODO *)
Admitted.

(* ----- PROBLEM 2 (b) -----

   Prove that iterating the higher-order function works. *)

Fixpoint fibonacci (n : nat) : nat :=
  match n with
  | 0 => 0
  | S n' => match n' with
            | 0 => 1
            | S n'' => (fibonacci n') + (fibonacci n'')
            end
  end.

Theorem fib_equiv :
  forall (i n : nat), n <= i+1 -> (makeFibI (i+1)) n = Some (fibonacci n).
Proof.
(* TODO *)
Admitted.