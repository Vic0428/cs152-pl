Require Import ZArith String Equality.
Open Scope string.

Definition Var := string.
Definition Var_eq := String.eqb.
Definition Int := Z.
Definition Int_add := Z.add.
Definition Int_mul := Z.mul.

Definition Store := Var -> Int.
Definition StoreUpdate (s : Store) (x : Var) (n : Int) : Store :=
  fun (y : Var) => if (Var_eq x y) then n else (s y).

(* ----- Denotational semantics for regular IMP ----- *)

Section OrigDenot.
  Inductive AExp :=
  | AEVar : Var -> AExp
  | AEInt : Int -> AExp
  | AEAdd : AExp -> AExp -> AExp
  | AEMul : AExp -> AExp -> AExp.

  Inductive BExp :=
  | Btrue : BExp
  | Bfalse : BExp
  | BLt : AExp -> AExp -> BExp.

  Inductive Com :=
  | CSkip : Com
  | CAsst : Var -> AExp -> Com
  | CSeq : Com -> Com -> Com
  | CITE : BExp -> Com -> Com -> Com
  | CWhile : BExp -> Com -> Com.

  Fixpoint A (a : AExp) (s : Store) : Z :=
    match a with
    | AEVar x => (s x)
    | AEInt n => n
    | AEAdd a1 a2 => (A a1 s) + (A a2 s)
    | AEMul a1 a2 => (A a1 s) * (A a2 s)
    end.

  Fixpoint B (b : BExp) (s : Store) : bool :=
    match b with
    | Btrue => true
    | Bfalse => false
    | BLt a1 a2 => Z.ltb (A a1 s) (A a2 s)
    end.

  (* Because computations in Coq have to terminate, and IMP might not terminate,
     we have to use a `fuel` parameter that causes the computation to time out
     (return `None`) if it recurses too many times.

     Statements about the denotational semantics are therefore phrased as:
       c : Com
       s s' : Store
       ... exists fuel, C fuel c (Some s) = Some s'. *)
  Fixpoint C (fuel : nat) (c : Com) (os : option Store) : option Store :=
    match fuel with
    | O => os
    | S n' => match os with
              | None => None
              | Some s => match c with
                          | CSkip => Some s
                          | CAsst x a => Some (StoreUpdate s x (A a s))
                          | CSeq c1 c2 => (C n' c2 (C n' c1 os))
                          | CITE b c1 c2 => match (B b s) with
                                            | true => C n' c1 os
                                            | false => C n' c2 os
                                            end
                          | CWhile b c' => match (B b s) with
                                           | true => (C n' c (C n' c' os))
                                           | false => Some s
                                           end
                          end
              end
    end.
End OrigDenot.

Reset AExp.

(* ----- PROBLEM 2 (c, d, e) -----

   Write a denotational semantics for IMP extended with pre- and post-increment. *)

Section NewDenot.
  Inductive AExp :=
  | AEVar : Var -> AExp
  | AEInt : Int -> AExp
  | AEAdd : AExp -> AExp -> AExp
  | AEMul : AExp -> AExp -> AExp
  | AELInc : Var -> AExp
  | AERInc : Var -> AExp.

  Inductive BExp :=
  | Btrue : BExp
  | Bfalse : BExp
  | BLt : AExp -> AExp -> BExp.

  Inductive Com :=
  | CSkip : Com
  | CAsst : Var -> AExp -> Com
  | CSeq : Com -> Com -> Com
  | CITE : BExp -> Com -> Com -> Com
  | CWhile : BExp -> Com -> Com.

  Fixpoint A (a : AExp) (* TODO *) := False.

  Fixpoint B (b : BExp) (* TODO *) := False.

  Fixpoint C (fuel : nat) (c : Com) (* TODO *) := False.
End NewDenot.

(* ----- PROBLEM 2 (a, b) -----

   Write and prove statements about the behavior of `progA` and `progB`. *)

Notation "a ; b" := (CSeq a b) (at level 20).
Definition sigma0 : Store := fun x => Z0.

Definition foo := "foo".
Definition bar := "bar".

Definition progA := CWhile (BLt (AELInc foo) (AEInt (Zpos 152))) (CAsst foo (AEMul (AELInc foo) (AEVar bar)); CAsst bar (AEMul (AELInc bar) (AEVar foo))).
Definition progB := CWhile (BLt (AEVar foo) (AEInt (Zpos 6))) (CAsst bar (AEAdd (AEVar bar) (AERInc foo))).

(* ----- CHALLENGE -----

   Write a large-step semantics for IMP with pre/post-increment and prove
   it equivalent to your denotational semantics. The equivalence theorem 
   should look something like:
     forall c s s', CLStep (c, s) s' <-> exists f, C f (Some s) = Some s'. *)

Inductive CLStep : (Com * Store) -> Store -> Prop := (* TODO *).