(* Implement the server operations *)

exception BadCmd
    
module DBMox = Map.Make(struct
  type t = string
  let compare = Pervasives.compare
end)
    
let room_assn : (string DBMox.t) ref = ref DBMox.empty
let _ =
  room_assn := DBMox.add "Marshall" "Mather 40" !room_assn;
  room_assn := DBMox.add "Jane" "Mather 41" !room_assn;
  room_assn := DBMox.add "Josephine" "Mather 41" !room_assn;
  room_assn := DBMox.add "Joanne" "Mather 42" !room_assn;
  room_assn := DBMox.add "Kevin" "Mather 42" !room_assn;
  room_assn := DBMox.add "Dan" "Currier 8" !room_assn;
  room_assn := DBMox.add "Lucas" "Currier 16" !room_assn;
  room_assn := DBMox.add "Jao-ke" "Currier 32" !room_assn;
  room_assn := DBMox.add "Julia" "Currier 32" !room_assn;
  room_assn := DBMox.add "Scott" "Currier 64" !room_assn;
  room_assn := DBMox.add "Steve" "Currier 64" !room_assn
      
let secret_pass : (string DBMox.t) ref = ref DBMox.empty
let _ =
  secret_pass := DBMox.add "Currier 8" "Currier 16" !secret_pass;
  secret_pass := DBMox.add "Currier 16" "Currier 32" !secret_pass;
  secret_pass := DBMox.add "Currier 32" "Currier 64" !secret_pass;
  secret_pass := DBMox.add "Mather 41" "Mather 42" !secret_pass

let num : int ref = ref 0
      
let do_server_op (op : string) (arg : string) : string =
  match op with
  | "echo" -> arg
  | "double" -> arg ^ arg
  | "GreetingService" -> "Hello " ^ arg
  | "getRoom" -> 
      (try DBMox.find arg !room_assn
      with Not_found -> "nonexistent entry")
  | "getSecretPassage" -> 
      (try DBMox.find arg !secret_pass
      with Not_found -> "nonexistent entry")
  | "getInt" ->
      let ans = !num in
      num := ans + 1;
      string_of_int ans
  | _ -> raise BadCmd
	
