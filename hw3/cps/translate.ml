open Ast

exception TODO of string

let rec cpsTranslate (src : source_exp) : target_exp =
  let k = freshvar () in
  let res = match src with
  | SE_Var v -> TE_App (TE_Var k, TE_Var v)
  | SE_Str s -> TE_App (TE_Var k, TE_Str s)
  | SE_Concat (s1, s2) -> TE_App (cpsTranslate s1, 
                                  let v = freshvar () in TE_Lam (v, TE_App(cpsTranslate s2, 
                                  let w = freshvar () in TE_Lam(w, 
                                  TE_App(TE_Var k, TE_Concat (TE_Var v, TE_Var w))))) )
  | SE_Lam (x, e) -> TE_App (TE_Var k, let k' = freshvar () in TE_Lam(x, TE_Lam(k', TE_App (cpsTranslate e, TE_Var k'))) )
  | SE_App (e1, e2) -> TE_App (cpsTranslate e1, let f = freshvar () in 
                               let v = freshvar () in TE_Lam(f, TE_App (cpsTranslate e2, TE_Lam (v, TE_App (TE_App (TE_Var f, TE_Var v),TE_Var k)))))
  | SE_Let (x, e1, e2) -> TE_App (cpsTranslate e1, let v = freshvar () in 
                               TE_Lam (v, TE_Let(x, TE_Var v, TE_App(cpsTranslate e2, TE_Var k))))
  | SE_Call (s, e) -> TE_App (cpsTranslate e, let v = freshvar () in 
                               TE_Lam (v, TE_Call(s, TE_Var v, TE_Var k)))
  in  TE_Lam (k, res)

