Require Import Coq.ZArith.BinInt.
Require Import Coq.Lists.List.
Require Import Coq.ZArith.Zdiv.
Require Import Coq.micromega.Lia.
Require Import Coq.ZArith.Znat.
Require Import FunInd.
Import ListNotations.
Require Import Coq.Program.Equality.

Inductive Com : Type :=
| Push : Z -> Com
| Pop : Com
| Add : Com
| Mul : Com
| Div : Com
| Dup : Com
| Swap : nat -> Com
| Skip : Com
| Seq : Com -> Com -> Com
| Whilene : Com -> Com.

Definition Stack : Set := list Z.
Definition Config : Set := Com * Stack.

Function swap' (i : nat) (n : Z) (s s' : Stack) : option Stack :=
  match s' with
  | nil => None
  | n' :: s'' => match i with
                 | O => Some (n' :: s ++ n :: s'')
                 | S i' => swap' i' n (s ++ (n' :: nil)) s''
                 end
  end.

Definition swap (i : nat) (s : Stack) : option Stack :=
  match s with
  | nil => None
  | n :: s' => match i with
               | O => Some s
               | S i' => swap' i' n nil s'
               end
  end.

(* the first parameter `d` is a "fuel" argument, because functions in Coq
   must be guaranteed to terminate. *)
Function interpret (d : nat) (c : Com) (s : Stack) : option Stack :=
    match c with
    | Push n => Some (n :: s)
    | Pop => match s with
             | n :: s' => Some s'
             | nil => None
             end
    | Add => match s with
             | n :: m :: s' => Some ((n+m)%Z :: s')
             | _ => None
             end
    | Mul => match s with
             | n :: m :: s' => Some ((n*m)%Z :: s')
             | _ => None
             end
    | Div => match s with
             | n :: m :: s' =>
                 match n with
                 | Z0 => None
                 | _ => Some ((m/n)%Z :: s')
                 end
             | _ => None
             end
    | Dup => match s with
             | n :: s' => Some (n :: n :: s')
             | nil => None
             end
    | Swap i => swap i s
    | Skip => Some s
    | Seq c1 c2 => match d with 
                   | O => None
                   | S d' =>
                       match (interpret d' c1 s) with
                       | Some s' => interpret d' c2 s'
                       | None => None
                       end
                   end
    | Whilene c' => match s with
                    | n :: m :: s' =>
                        match (Z.eqb n m) with
                        | true => Some s
                        | false =>
                            match d with
                            | O => None
                            | S d' =>
                                match (interpret d' c' s) with
                                | None => None
                                | Some s'' => interpret d' c s''
                                end
                            end
                        end
                    | _ => None
                    end
    end.

(* ----- PROBLEM 4 (a) -----

   Add inference rules to `lstep` as needed. *)

Inductive lstep : Config -> Stack -> Prop :=
| LSPop : forall n s,
            lstep (Pop, n :: s) s
| LSSwap : forall i s s',
             swap i s = Some s' ->
               lstep (Swap i, s) s'
| LSDiv : forall n m s,
            n <> 0%Z ->
              lstep (Div, n :: m :: s) ((m/n)%Z :: s)
| LSSeq : forall c1 c2 s s' s'',
            lstep (c1, s) s' ->
            lstep (c2, s') s'' ->
              lstep (Seq c1 c2, s) s''
| LSWhilene : forall c n s,
                 lstep (Whilene c, n :: n :: s) (n :: n :: s).

(* ----- CHALLENGE -----

   Prove that your large step semantics are equivalent to the given interpreter. *)

Theorem lstep_correctness :
  forall c s s', lstep (c, s) s' <-> exists d, interpret d c s = Some s'.
Proof.
  intros.
  unfold iff. split.
  - intros. dependent induction H. 
    * exists 1. simpl. reflexivity.
    * exists 1. simpl. assumption.
    * exists 1. simpl. unfold not in H.
      destruct n. 
      ** assert (False). apply H. reflexivity. inversion H0.
      **  reflexivity.
      ** reflexivity.
    * 
      specialize (IHlstep1 c1 s).
      assert (exists d : nat, interpret d c1 s = Some s').
      { apply IHlstep1. reflexivity. }
      destruct H1.
      specialize (IHlstep2 c2 s').
      assert (exists d : nat, interpret d c2 s' = Some s'').
      { apply IHlstep2. reflexivity. }
      destruct H2.
      exists ( x + x0).
      simpl.
      destruct (interpret x c1 s).
      **  inversion H1. subst. 
     

 simpl.
      assert (n = 0%Z).
      {  auto.

 unfold neq in H.
Admitted.

(* ----- PROBLEM 4 (b) -----

   Give a configuration that gets stuck. *)

Example stuck_config :
  exists conf, forall s', ~(lstep conf s').
Proof.

Admitted.

(* ------------------------ *)

Notation "a ; b" := (Seq a b) (at level 20).
Definition nStack (n : nat) : Stack :=
  (Z.of_nat n) :: nil.
Definition nmStack (n m : nat) : Stack :=
  (Z.of_nat n) :: (Z.of_nat m) :: nil.

(* ----- PROBLEM 4 (c, d, e) -----

   Write programs that perform the computations described in the assignment. *)

Definition progC : Com :=
    (* replace with your program *)
    (Skip; Skip; Skip).

Definition progD : Com :=
    (* replace with your program *)
    (Skip; Skip; Skip).

Definition progE : Com :=
    (* replace with your program *)
    (Skip; Skip; Skip).

(* You can test your programs like this. *)
Eval compute in interpret 100 progC (nStack 10).

(* ----- CHALLENGE -----

   Prove that your stack programs are correct.
   There are several ways that you might specify correctness. *)


