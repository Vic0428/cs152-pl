Require Import Coq.ZArith.BinInt.
Require Import Coq.QArith.QArith_base.
Require Import Coq.Program.Equality.
Require Import Coq.micromega.Lia.

Inductive Tree : Set :=
  | L : Q -> Tree                  (* Leaf node *)
  | I : Q -> Tree -> Tree -> Tree. (* Internal node *)

(* ----- PROBLEM 2 (a) -----

   Fill in the inference rules of `Isomorphic` below. *)

Inductive Isomorphic : Tree -> Tree -> Prop :=
  | Rule1: forall a b c d, 
            Isomorphic (L (a # b)) (L (c # d))
  | Rule2: forall a b c d t1 t2 t3 t4,
            Isomorphic t1 t3 ->
            Isomorphic t2 t4 -> 
            Isomorphic (I (a # b) t1 t2) (I (c # d) t3 t4).


Definition root (t : Tree) :=
  match t with
  | L q => q
  | I q t' t'' => q
  end.

Inductive SixTree : Tree -> Prop :=
  | Leaf6   :   forall a b,
                Qeq (a # b) (6 # 1) ->
                  SixTree (L (a # b))
  | Leaf18  :   forall a b,
                Qeq (a # b) (18 # 1) ->
                  SixTree (L (a # b))
  | SumNode :   forall t1 t2 q,
                SixTree t1 ->
                SixTree t2 ->
                Qeq q (root t1 + root t2) ->
                  SixTree (I q t1 t2)
  | ProdNode :  forall t1 t2 q,
                SixTree t1 ->
                SixTree t2 ->
                Qeq q ((1 # 6) * root t1 * root t2) ->
                  SixTree (I q t1 t2).

(* ----- PROBLEM 2 (b) -----

   Formalize the example trees, and prove whether they're
   SixTrees. It may not be possible to formalize all the
   examples... *)

Example TreeA : SixTree (L (0 # 1)).
Proof.
Admitted.

Example TreeB : SixTree (I (24 # 1) (I (6 # 1) (L (6 # 1)) (L (6 # 1))) (L (18 # 1))).
Proof.
assert (SixTree (I (6 # 1) (L (6 # 1)) (L (6 # 1)))).
{ eapply ProdNode. apply Leaf6. reflexivity. apply Leaf6.  reflexivity. unfold root. reflexivity. }
eapply SumNode.
apply H.
apply Leaf18. reflexivity.
reflexivity.
Qed.
  

Example TreeC : SixTree (L (0 # 1)).
Proof.
Admitted.

Example TreeD : SixTree (L (0 # 1)).
Proof.
Admitted.

(* ----- PROBLEM 2 (c) -----

   Given an example of a SixTree with multiple derivations,
   and provide two alternate proofs of its membership. *)
Lemma Subtree: SixTree (I (12 # 1) (L (6 # 1)) (L (6 # 1))).
Proof.
apply SumNode.
apply Leaf6. reflexivity.
apply Leaf6. reflexivity.
reflexivity.
Qed.

Example TreeE : SixTree (I (24 # 1) (I (12 # 1) (L (6 # 1)) (L (6 # 1))) (I (12 # 1) (L (6 # 1)) (L (6 # 1)))).
Proof.
eapply SumNode.
apply Subtree.
apply Subtree.
reflexivity.
Restart.
eapply ProdNode.
apply Subtree.
apply Subtree.
reflexivity.
Qed.


(* ----- PROBLEM 2 (d) -----

   Prove that the root of every SixTree holds an even integer value. *)


(* For this question, I haven't finished it. It's because I am stuck in the qarithmatic part*)

Lemma SixTree_root_non_zero: forall (t : Tree),
  SixTree t -> ~ (root t) == 0.
Proof.
  intros.
  dependent induction H.
  * unfold root. unfold not. intros.
    apply Qeq_sym in H0.
    assert (0 == 6).
    { eapply Qeq_trans. apply H0. assumption. }
    inversion H1.

  * unfold root. unfold not. intros.
    apply Qeq_sym in H0.
    assert (0 == 18).
    { eapply Qeq_trans. apply H0. assumption. }
    inversion H1.
  * 
Admitted.
    

Theorem SixTree_root_even (t : Tree) :
  SixTree t -> exists (i : Z), root t == (i # 1) * (2 # 1).
Proof.
  assert (SixTree t -> exists i : Z, root t == (i # 1) * 6).
  { intros. induction H. 
    * unfold root.
      exists 1%Z.
      auto.
    * unfold root. exists 3%Z. auto.
    * unfold root. destruct IHSixTree1. destruct IHSixTree2.
      assert (q == (x # 1) * 6 + (x0 # 1) * 6).
      { eapply Qeq_trans. apply H1.
        assert (root t1 + root t2 == (x # 1) * 6 + root t2).
        {apply Qplus_inj_r. assumption. }
        eapply Qeq_trans. apply H4. apply Qplus_inj_l. assumption. }
        assert ( ((x # 1) + (x0 # 1)) * 6 == (x # 1) * 6 + (x0 # 1) * 6).
        { apply Qmult_plus_distr_l. }
        assert ((x # 1) + (x0 # 1) == (x + x0) # 1).
        { apply Qinv_plus_distr. }
        assert (((x # 1) + (x0 # 1)) * 6 == (x + x0 # 1) * 6).
        { apply Qmult_inj_r . 
          apply Qeq_bool_neq. auto.
          assumption. }
        assert (q == (x + x0 # 1) * 6).
        { eapply Qeq_trans. apply H4. apply Qeq_sym in H5.
          eapply Qeq_trans. apply H5. assumption. }
        exists ((x + x0)%Z).
        assumption.
    * unfold root. destruct IHSixTree1. destruct IHSixTree2.
      assert (root t1 * root t2 == ((x # 1) * 6) * root t2).
      { apply Qmult_inj_r. apply SixTree_root_non_zero. assumption. }
      assert ((x # 1) * 6 * root t2 == (x # 1) * 6 * ((x0 # 1) * 6)).
      { apply Qmult_inj_l with (z := (x # 1) * 6) . unfold not. intros. 
      assert (root t1 == 0).
      { eapply Qeq_trans. apply H2. assumption. }
      assert (~ (root t1 == 0)).
      {apply SixTree_root_non_zero. assumption. }
      apply H7. assumption. }
      assert ( root t1 * root t2 == (x # 1) * 6 * ((x0 # 1) * 6)).
      { eapply Qeq_trans. apply H4. apply H5. }
      assert ( (1 # 6) * (root t1 * root t2) == (1 # 6) * ((x # 1) * 6 * ((x0 # 1) * 6))). 
      {  apply Qmult_inj_l. unfold not. intros. inversion H7. assumption. }
      assert ((1 # 6) * (root t1 * root t2) == (1 # 6) * root t1 * root t2).
      { apply Qmult_assoc. }
      apply Qeq_sym in H8.
      assert (q == (1 # 6) * ((x # 1) * 6 * ((x0 # 1) * 6))).
      { eapply Qeq_trans. apply H1. eapply Qeq_trans. apply H8. assumption. }
      assert (q == (1 # 6) * ((x # 1) * 6) * ((x0 # 1) * 6)).
      { eapply Qeq_trans. apply H9. apply Qmult_assoc. }
      assert ( q == (((1 # 6) * (x # 1) * 6)) * ((x0 # 1) * 6)).
      { eapply Qeq_trans.  apply H10. apply Qmult_inj_r. 
        unfold not. intros. assert ( root t2 == 0). { eapply Qeq_trans. apply H3. assumption. }
        assert (~ (root t2 == 0)). { apply SixTree_root_non_zero. assumption. }
        apply H13. assumption. }
    



Admitted.


(* ----- PROBLEM 2 (e) -----

   Prove that `Isomorphic` is transitive. *)

Theorem Isomorphic_trans (t1 t2 t3 : Tree) :
  Isomorphic t1 t2 -> Isomorphic t2 t3 -> Isomorphic t1 t3.
Proof.
intros.
generalize dependent t3.
dependent induction H.
- intros. inversion H0. subst. apply Rule1.
- intros. inversion H1. subst.
  apply Rule2.
  * apply IHIsomorphic1. assumption.
  * apply IHIsomorphic2. assumption.
Qed.
