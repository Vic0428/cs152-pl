From Coq Require Import ZArith.ZArith.
From Coq Require Import Strings.String.
Open Scope string_scope.
Require Import Coq.Program.Equality.

Definition Var := string.
Definition Var_eq := String.eqb.
Definition Int := Z.
Definition Int_add := Z.add.
Definition Int_mul := Z.mul.
Definition Int_mod := Zmod.
Definition Int_fact := fun n => if (n <? 1)%Z then Zpos 1
                                else (Z.of_nat (fact (Z.to_nat n))).

Inductive Exp : Type :=
| EVar : Var -> Exp
| EInt : Int -> Exp
| EAdd : Exp -> Exp -> Exp
| EMul : Exp -> Exp -> Exp
| EAsg : Var -> Exp -> Exp -> Exp

(* ----- New constructors ----- *)

| EFact : Exp -> Exp
| EMod : Exp -> Exp -> Exp.

Definition Store := Var -> Int.

Definition Config := (Exp * Store)%type.
Definition FinalConfig := (Int * Store)%type.

Definition StoreUpdate (s : Store) (x : Var) (n : Int) : Store :=
  fun (y : Var) => if (Var_eq x y) then n else (s y).

Implicit Types (n m : Int) (x : Var) (e f g : Exp) (s t u : Store).
Reserved Notation "c1 '-->' c2" (at level 40).

Inductive lstep : Config -> FinalConfig -> Prop :=
| LSInt : forall n s,
            lstep (EInt n, s) (n, s)
| LSVar : forall x s,
            lstep (EVar x, s) (s x, s)
| LSAdd : forall n m e f s t u,
            lstep (e, s) (n, t) ->
            lstep (f, t) (m, u) ->
              lstep (EAdd e f, s) (Int_add n m, u)
| LSMul : forall n m e f s t u,
            lstep (e, s) (n, t) ->
            lstep (f, t) (m, u) ->
              lstep (EMul e f, s) (Int_mul n m, u)
| LSAsg : forall n m x e f s t u,
             lstep (e, s) (n, t) ->
             lstep (f, StoreUpdate t x n) (m, u) ->
               lstep (EAsg x e f, s) (m, u)
| LSFact : forall n e s t,
             lstep (e, s) (n, t) ->
               lstep (EFact e, s) (Int_fact n, t)
| LSMod : forall e f m n s t u,
            (n > 0)%Z ->
            lstep (e, s) (m, t) ->
            lstep (f, t) (n, u) ->
              lstep (EMod e f, s) (Int_mod m n, u).

(* ----- PROBLEM 1 (a, b) -----

   Extend `sstep` below to handle factorial and modulo `Exp`s,
   agreeing with the large-step semantics above. *)

Inductive sstep : Config -> Config -> Prop :=
| SSVar : forall x s,
            (EVar x, s) --> (EInt (s x), s)
| SSAdd : forall n m s,
            (EAdd (EInt n) (EInt m), s) --> (EInt (Int_add n m), s)
| SSMul : forall n m s,
            (EMul (EInt n) (EInt m), s) --> (EInt (Int_mul n m), s)
| SSAsg : forall n x e s,
             (EAsg x (EInt n) e, s) --> (e, StoreUpdate s x n)
| SSLAdd : forall e f g s t,
             (e, s) --> (f, t)  ->
               (EAdd e g, s) --> (EAdd f g, t)
| SSRAdd : forall n e f s t,
             (e, s) --> (f, t)  ->
               (EAdd (EInt n) e, s) --> (EAdd (EInt n) f, t)
| SSLMul : forall e f g s t,
             (e, s) --> (f, t)  ->
               (EMul e g, s) --> (EMul f g, t)
| SSRMul : forall n e f s t,
             (e, s) --> (f, t)  ->
               (EMul (EInt n) e, s) --> (EMul (EInt n) f, t)
| SSAsg1 : forall x e f g s t,
              (e, s) --> (f, t)  ->
                (EAsg x e g, s) --> (EAsg x f g, t)
| SSFact1: forall e s f t,
              (e, s) --> (f, t)  -> 
                (EFact e, s)    --> (EFact f, t)
| SSFact2: forall n s,
                (EFact (EInt n), s) --> (EInt (Int_fact n), s)
| SSLMod : forall e1 e2 s f t,
              (e1, s) --> (f, t) ->
                 (EMod e1 e2, s) --> (EMod f e2, t)
| SSRMod : forall n e2 s f t,
              (e2, s) --> (f, t) ->
                 (EMod (EInt n) e2, s) --> (EMod (EInt n) f, t)
| SSMod  : forall m n s,
              (n > 0)%Z -> (EMod (EInt m) (EInt n), s) --> (EInt (Int_mod m n), s) 



(* ----- Your constructors here ----- *)

where "c1 '-->' c2" := (sstep c1 c2).

Definition relation (X : Type) := X -> X -> Prop.

Inductive multi {X : Type} (R : relation X) : relation X :=
  | multi_refl : forall (x : X), multi R x x
  | multi_step : forall (x y z : X),
                    R x y ->
                    multi R y z ->
                    multi R x z.

Definition multisstep := (multi sstep).
Notation "t1 '-->*' t2" := (multisstep t1 t2) (at level 40).


Lemma multi_split: forall {X: Type} (R: relation X) (x y z: X),
    multi R x y -> multi R y z -> multi R x z.
Proof.
  intros. dependent induction H.
  * assumption.
  * eapply multi_step.  apply H. apply IHmulti. assumption.
Qed. 
(* ----- CHALLENGE -----

   Prove that your answer to 1(a,b) is correct. *)
Lemma lemma1_ladd: forall e s n t e2,
  (e, s) -->* (EInt n, t) ->
    (EAdd e e2, s) -->* (EAdd (EInt n) e2, t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSLAdd. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_lmul: forall e s n t e2,
  (e, s) -->* (EInt n, t) ->
    (EMul e e2, s) -->* (EMul (EInt n) e2, t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSLMul. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_lmod: forall e s n t e2,
  (e, s) -->* (EInt n, t) ->
    (EMod e e2, s) -->* (EMod (EInt n) e2, t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSLMod. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.


Lemma lemma1_radd: forall e s n t m,
  (e, s) -->* (EInt n, t) ->
    (EAdd (EInt m) e, s) -->* (EAdd (EInt m) (EInt n), t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSRAdd. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_rmul: forall e s n t m,
  (e, s) -->* (EInt n, t) ->
    (EMul (EInt m) e, s) -->* (EMul (EInt m) (EInt n), t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSRMul. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_rmod: forall e s n t m,
  (e, s) -->* (EInt n, t) ->
    (EMod (EInt m) e, s) -->* (EMod (EInt m) (EInt n), t).
Proof.
  intros. dependent induction H.
  *  apply multi_refl.
  * destruct y.
    eapply multi_step.
    ** apply SSRMod. apply H.
    ** apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_asg: forall e s n t x f,
  (e, s) -->* (EInt n, t) ->
    (EAsg x e f, s) -->* (EAsg x (EInt n) f, t).
Proof.
   intros. dependent induction H.
  * apply multi_refl.
  * destruct y.
    eapply multi_step.
    **  apply SSAsg1. apply H.
    **  apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma1_fact: forall e s n t,
  (e, s) -->* (EInt n, t) ->
    (EFact e, s) -->* (EFact (EInt n), t).
Proof.
   intros. dependent induction H.
  * apply multi_refl.
  * destruct y.
    eapply multi_step.
    **  apply SSFact1. apply H.
    **  apply IHmulti. reflexivity. reflexivity.
Qed.

Lemma lemma2: forall e s e0 s0 n s2,
  (e, s) --> (e0, s0) ->
    (lstep (e0, s0) (n, s2)) -> (lstep (e, s) (n, s2)).

Proof.
  intros.
  generalize dependent n.
  generalize dependent s2.
  dependent induction H.
  * intros. inversion H0. subst. apply LSVar.
  * intros. inversion H0. subst.
    apply LSAdd with (t := s2); apply LSInt.
  * intros. inversion H0. subst.
    apply LSMul with (t := s2); apply LSInt.
  * intros.
    apply LSAsg with (n := n) (t := s).
    apply LSInt. apply H0.
  * intros.
    inversion H0. subst.
    specialize (IHsstep e1 s f s0).
    assert (          forall s2 n,
          lstep (f, s0) (n, s2) -> lstep (e1, s) (n, s2)).
    {apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (n0, t)).
    { apply H1.  assumption. }
    eapply LSAdd.
    apply H2. apply H7.
  * intros.
    inversion H0. subst. inversion H3. subst.
    specialize (IHsstep e1 s f t).
    assert (lstep (f, t) (m, s2) -> lstep (e1, s) (m, s2)).
    { apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (m, s2)).
    { apply H1. assumption. }
    eapply LSAdd.
    apply LSInt.
    apply H2.
  * intros.
    inversion H0. subst.
    specialize (IHsstep e1 s f s0).
    assert (          forall s2 n,
          lstep (f, s0) (n, s2) -> lstep (e1, s) (n, s2)).
    {apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (n0, t)).
    { apply H1.  assumption. }
    eapply LSMul.
    apply H2.  apply H7.
  * intros.
    inversion H0. subst. inversion H3. subst.
    specialize (IHsstep e1 s f t).
    assert (lstep (f, t) (m, s2) -> lstep (e1, s) (m, s2)).
    { apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (m, s2)).
    { apply H1. assumption. }
    eapply LSMul.
    apply LSInt.
    apply H2.
  * intros.
    inversion H0. subst.
    specialize (IHsstep e1 s f s0).
    assert (lstep (f, s0) (n0, t) -> lstep (e1, s) (n0, t)).
    { apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (n0, t)).
    { apply H1. assumption. }
    eapply LSAsg. apply H2. apply H8.
  * intros. inversion H0. subst.
    specialize (IHsstep e1 s f s0).
    assert (lstep (e1, s) (n0, s2)).
    { apply IHsstep. reflexivity. reflexivity. apply H2. }
    eapply LSFact. assumption.
  * intros. inversion H0. subst. apply LSFact. apply LSInt.
  * intros.
    inversion H0. subst.
    specialize (IHsstep e1 s f s0).
    assert (          forall s2 n,
          lstep (f, s0) (n, s2) -> lstep (e1, s) (n, s2)).
    {apply IHsstep; reflexivity. }
    assert (lstep (e1, s) (m, t)).
    { apply H1.  assumption. }
    eapply LSMod.
    apply H5. apply H2.  apply H8.
  * intros.
    inversion H0. subst. inversion H7. subst.
    specialize (IHsstep e2 s f t).
    assert (lstep (e2, s) (n1, s2)).
    { apply IHsstep. reflexivity. reflexivity. assumption. }
    eapply LSMod.
    apply H5.  apply LSInt. assumption.
  * intros. inversion H0. subst.
    eapply LSMod.
    assumption. apply LSInt. apply LSInt.
Qed.


Theorem semantic_equivalence :
  forall e n s t, lstep (e, s) (n, t) <-> (e, s) -->* (EInt n, t).
Proof.
  unfold iff. split.
  - intros. dependent induction H.
    ** apply multi_refl.
    ** eapply multi_step. apply SSVar. apply multi_refl.
    ** specialize (IHlstep1 e0 n0 s t0). 
       specialize (IHlstep2 f m t0 t).
       assert ((e0, s) -->* (EInt n0, t0)). apply IHlstep1. reflexivity. reflexivity.
       assert ((f, t0) -->* (EInt m, t)). apply IHlstep2. reflexivity. reflexivity.
       assert ((EAdd e0 f, s) -->* (EAdd (EInt n0) f, t0)). apply lemma1_ladd. apply H1.
       assert ((EAdd (EInt n0) f, t0) -->* (EAdd (EInt n0) (EInt m), t)). apply lemma1_radd. apply H2.
       assert ((EAdd (EInt n0) (EInt m), t) -->* (EInt (Int_add n0 m), t)). 
       { eapply multi_step. apply SSAdd. apply multi_refl. }
       eapply multi_split. apply H3. eapply multi_split. apply H4. apply H5.
    ** specialize (IHlstep1 e0 n0 s t0). 
       specialize (IHlstep2 f m t0 t).
       assert ((e0, s) -->* (EInt n0, t0)). apply IHlstep1. reflexivity. reflexivity.
       assert ((f, t0) -->* (EInt m, t)). apply IHlstep2. reflexivity. reflexivity.
       assert ((EMul e0 f, s) -->* (EMul (EInt n0) f, t0)). apply lemma1_lmul. apply H1.
       assert ((EMul (EInt n0) f, t0) -->* (EMul (EInt n0) (EInt m), t)). apply lemma1_rmul. apply H2.
       assert ((EMul (EInt n0) (EInt m), t) -->* (EInt (Int_mul n0 m), t)). 
       { eapply multi_step. apply SSMul. apply multi_refl. }
       eapply multi_split. apply H3. eapply multi_split. apply H4. apply H5.
    ** specialize (IHlstep1 e0 n0 s t0).
       remember (StoreUpdate t0 x n0) as ss.
       specialize (IHlstep2 f n ss t).
       assert ((e0, s) -->* (EInt n0, t0)).
       { apply IHlstep1. reflexivity. reflexivity. }
       assert ((f, ss) -->* (EInt n, t)).
       { apply IHlstep2. reflexivity. reflexivity. }
       assert ((EAsg x e0 f, s) -->* (EAsg x (EInt n0) f, t0)). 
       { apply lemma1_asg. apply H1. }
       assert ((EAsg x (EInt n0) f, t0) -->* (EInt n, t)).
       { eapply multi_step. apply SSAsg. subst. apply H2. }
       eapply multi_split. apply H3. apply H4.
    ** specialize (IHlstep e0 n0 s t).
       assert ((e0, s) -->* (EInt n0, t)).
       { apply IHlstep. reflexivity. reflexivity. }
       assert ((EFact e0, s) -->* (EFact (EInt n0), t)).
       { apply lemma1_fact. apply H0. }
       eapply multi_split.
       apply H1.
       eapply multi_step.
       apply SSFact2.
       apply multi_refl.
    ** specialize (IHlstep1 e0 m s t0). 
       specialize (IHlstep2 f n0 t0 t).
       assert ((e0, s) -->* (EInt m, t0)).
       { apply IHlstep1. reflexivity. reflexivity. }
       assert ((f, t0) -->* (EInt n0, t)).
       { apply IHlstep2. reflexivity. reflexivity. }
       assert ((EMod e0 f, s) -->* (EMod (EInt m) f, t0)).
       { apply lemma1_lmod. apply H2. }
       assert (((EMod (EInt m) f, t0) -->* (EMod (EInt m) (EInt n0), t))).
       { apply lemma1_rmod. apply H3. }
       eapply multi_split.
       apply H4. eapply multi_split.
       apply H5. eapply multi_step.
       apply SSMod. assumption. apply multi_refl.
 - intros. dependent induction H.
   * apply LSInt.
   * destruct y.
     specialize (IHmulti e0 n s0 t).  
     assert (lstep (e0, s0) (n, t)).
     { apply IHmulti. reflexivity. reflexivity. }
     eapply lemma2.
     apply H.
     apply H1.
Qed.
      


(* ----- PROBLEM 1 (c) -----

   Formalize the example <(fact 3) mod (-7), s0> and prove something
   about its behavior in the large and small step semantics. *)

(* For small step semantics, we prove it can be derived to <6 mod (-7), s0> *)
Theorem ss_p1:
  forall s,
  (EMod (EFact (EInt (3%Z))) (EInt (-7)%Z), s) -->* (EMod (EInt (6%Z)) (EInt  (-7)%Z), s).
Proof.
  intros.
  eapply multi_step.
  eapply SSLMod. apply SSFact2.
  apply multi_refl.
Qed.


(* But it won't evaluate to final configuration *)
Theorem ss_p2:
  forall s n t,
  (EMod (EInt (6%Z)) (EInt  (-7)%Z), s) -->* (EInt n, t) -> False.
Proof.
  intros.  
  inversion H. 
  inversion H0.
  subst.
  *  inversion H8.
  *  subst. inversion H8.
  *  subst. inversion H8.
Qed.


Theorem ls_p1:
  forall s n t,
  lstep (EMod (EFact (EInt (3%Z))) (EInt (-7)%Z), s) (n, t) -> False.
Proof.
  intros.
  inversion H.
  subst.
  inversion H7.
  subst.
  inversion H4.
Qed.
