open Ast
open Helper

exception TypeError 
exception UnificationError
exception UnimplementedError

(* [unify c0] solves [c0] (if possible), yielding a substitution. Raise UnificationError if constraints cannot be unified. *)
let rec unify (c0:constr) : subst = 
  (* FILL IN HERE FOR QUESTION 4 *)
  if Constr.is_empty c0 then VarMap.empty 
    else (
      (* Pick one condition tau == tau' from the c0 *)
      let tau, tau_prime = Constr.choose c0 in
      (* Remove this condition from the c0 to get c *)
      let c = (Constr.remove (tau, tau_prime) c0) in
      if tau = tau_prime then unify c else
      (
        match tau with
        | TVar x -> (
          let free_vs = ftvs tau_prime in 
          if VarSet.mem x free_vs then (
            match tau_prime with
            | TVar x -> (
              let free_vs = ftvs tau in 
              if VarSet.mem x free_vs then raise UnificationError 
              else VarMap.add x tau (unify (subst_constr c x tau))
            )
            | _ -> raise UnificationError
          ) else (
            VarMap.add x tau_prime (unify (subst_constr c x tau_prime))
          )
        )
        | _ -> (
          match tau_prime with 
          | TVar x -> (
            let free_vs = ftvs tau in 
            if VarSet.mem x free_vs then raise UnificationError 
            else VarMap.add x tau (unify (subst_constr c x tau))
          )
          | _ -> (
            match tau, tau_prime with 
            | TArrow (tau0, tau1), TArrow (tau0', tau1') |
              TPair (tau0, tau1), TPair (tau0', tau1')
            -> (
              unify (Constr.add (tau0, tau0') (Constr.add (tau1, tau1') c))
            )
            | _ -> raise UnificationError
          )
        )
      )
    )

(* [check g e0] typechecks [e0] in the context [g] generating a type and a set of constraints. Raise TypeError if constraints cannot be generated. *)
let rec check (g:context) (e0:exp) : typ * constr = 
  match e0 with
    | Var x -> if VarMap.mem x g then VarMap.find x g, Constr.empty  else raise TypeError
    | App (e1, e2) -> (let tau1, c1 = check g e1 in 
                       let tau2, c2 = check g e2 in  
                       let x = next_tvar() in
                       x,  Constr.union c1 (Constr.add (tau1, TArrow (tau2, x)) c2)
    )
    | Lam (x, e) -> (let tau1 = next_tvar() in
                     let tau2, c = check (VarMap.add x tau1 g) e in 
                     TArrow (tau1, tau2), c)
    | Let (x, e1, e2) -> (let tau1, c1 = check g e1 in 
                          let tau2, c2 = check (VarMap.add x tau1 g) e2 in
                          tau2, Constr.union c1 c2
                          )
    | Int n -> TInt, Constr.empty
    | Plus (e1, e2) | Times (e1, e2) | Minus (e1, e2) -> (let tau1, c1 = check g e1 in
                        let tau2, c2 = check g e2 in
                        TInt, Constr.union c1 (Constr.add (tau1, TInt) (Constr.add (tau2, TInt) c2))
                        )
    | Pair (e1, e2) -> (
      let tau1, c1 = check g e1 in
                        let tau2, c2 = check g e2 in
                        TPair (tau1, tau2), Constr.union c1 c2
                        )
    | Fst e -> 
    ( 
    let tau1', tau2' = next_tvar(), next_tvar() in
    let tau, c = check g e in (
      match tau with 
      | TPair (tau1, tau2) -> tau2', Constr.add (tau, TPair (tau2', tau1')) c
      | _ -> raise TypeError
    )
    )
    | Snd e ->
    ( 
    let tau1', tau2' = next_tvar(), next_tvar() in
     let tau, c = check g e in (
      match tau with 
      | TPair (tau1, tau2) -> tau1', Constr.add (tau, TPair (tau2', tau1')) c
      | _ -> raise TypeError
    )
    )
    | True | False -> TBool, Constr.empty
    | Eq (e1, e2) -> (let tau1, c1 = check g e1 in
                        let tau2, c2 = check g e2 in
                        TBool, Constr.union c1 (Constr.add (tau1, TInt) (Constr.add (tau2, TInt) c2))
                        )
    | If (e1, e2, e3) -> (
      let tau1, c1 = check g e1 in
      let tau2, c2 = check g e2 in 
      let tau3, c3 = check g e3 in 
      tau2, Constr.union c1 (Constr.union c2 (Constr.add (tau1, TBool) (Constr.add (tau2, tau3) c3)))
    ) 
    | Letrec (f, x, e1, e2) -> (
      let tau = next_tvar() in
      let tau' = next_tvar() in 
      let tau1, c1 = check (VarMap.add x tau (VarMap.add f (TArrow (tau, tau')) g)) e1 in
      let tau2, c2 = check (VarMap.add f (TArrow (tau, tau')) g) e2 in 
      tau2, Constr.union c1 (Constr.add (tau', tau1) c2)
    )
