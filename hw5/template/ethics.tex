\documentclass[boxanswers,bluebook]{article}
\usepackage{amsmath}
\usepackage{plcourse}
\usepackage{answer}
\usepackage{palatino}
\usepackage{hyperref}
\usepackage{xspace}
\usepackage{tikz}
\usepackage{enumerate}
\usetikzlibrary{arrows,positioning}

\homework{5: Embedded EthiCS}
\due{Thursday, April 22, 2021, \textbf{11:59{\textsc{pm} \small Eastern Daylight Time}}}
\newif\ifshowquestions\showquestionstrue
\ifsolution\showquestionsfalse \fi


\begin{document}

\ifsolution\solnheader\else\hwheader\fi
\ifsolution\showquestionsfalse \fi
%\section*{What to turn in}
\ifshowquestions
This is a supplemental question for Assignment 5, for the CS 152 \href{https://embeddedethics.seas.harvard.edu/}{Embedded EthiCS} module.}.


\textbf{Submission instructions:}
Please submit your homework as a PDF via GradeScope. You do not need to anonymize your submission, although you are welcome to if you would like.


\begin{question}{Ethical Specifications for Lethal Automated Weapon Systems}{}


\begin{center}
         \fbox{\parbox{0.96\textwidth}{
{\large \textbf{Content Warning (please read attentively)}}
You are about to be presented with a fictional scenario that touches on issues of war and the loss of human lives. Please be forewarned that the content of this assignment is highly sensitive, and you will be faced with difficult (though fictional) choices. When drafting your response to this assignment, and in discussion, remember that some of your classmates may have been directly or indirectly affected by automated weapon systems. We therefore ask that you choose your words with caution and sensitivity. These are difficult questions which you may need to confront in the future. We appreciate your effort in addressing them!
           }}
\end{center}


Imagine the following hypothetical scenario:

You are a private contractor working for the Department of Defense on the development of semi-autonomous lethal weapon systems. Your work consists in programming semi-autonomous drones that help the military optimize their operations. Your work improves the accuracy of lethal weapons and enhances the effectiveness of missions and should thus minimize civilian casualties and the risk to U.S. soldiers.

	The DoD informs your employer about Directive 3000.09, a series of regulations it adopted in 2012, which have been updated by every Secretary of Defense since. The ``Centaur Principle,'' as these regulations are jointly known, rules that the U.S. military is not allowed to automate the decision to use lethal force under any circumstance. It allows semi-autonomous weapon systems to be deployed for lethal purposes only when there is a human ``in the loop'' who takes responsibility for any such decision. In other words, while it allows tasks to be automated, it requires decisions to use lethal force to be owned by humans.
	The Secretary of Defense has emphasized to your employer that any failure to abide by the Centaur Principle constitutes a violation of domestic and international law. She warned you and your colleagues that you can only program drones to engage individual targets that have been previously selected by a human operator. She further insisted that the employment of lethal force must be constantly overseen by human agents.
        Based on this directive, your company has programmed semi-autonomous drones to employ lethal force solely on the basis of human commands. Furthermore, you have made sure that human operators can always override prior orders given to the system. Absent human oversight, drones are programmed to abort their tasks and return to base.

	One day you come into work to learn that a military operation that employed the drones you had programmed was seriously compromised when the drone's network connection dropped. The drone operators had identified and engaged a target, with the aim of preventing an attack on a U.S. military base. After communications were lost, the drone aborted the mission and returned to base. The attack resulted in several military and civilian casualties. The DoD is now requesting that you reprogram the drone to prevent this kind of situation in the future.
        You are now tasked with producing the code that will determine the drone's course of action after a human operator has initiated a strike. More specifically, you are required to implement a function that is invoked (repeatedly) after a strike has been initiated but before the missile is fired. This function takes as input the current network latency (including whether the connection has dropped) and any changes in the physical environment since the strike was initiated (for example, any additional entities that have entered the field of view, and estimates of whether the entities are non-combatants, friendly combatants, or enemy combatants). The function must return a Boolean, indicating whether the drone should continue with the strike or abort. The network latency may be helpful as a proxy for whether the drone operator is providing human oversight.

	As far as you know, Directive 3000.09 has not been changed or updated. This means that the Centaur Principle remains a red line. You are given, however, a list of ethical principles which you are asked to take into consideration when designing the algorithm. Together, these principles are known as the doctrine of just war:
        \begin{itemize}
        \item  \emph{Right to self-defense:} Self-defense against physical aggression (whether already committed or simply anticipated) constitutes a just cause for aggression.
\item	\emph{Discrimination:} When using lethal force, agents must discriminate between combatants and non-combatants. The latter ought not to be targeted, though collateral harm to them is permissible under the following circumstances:
\item	\emph{Proportionality:} Collaterally harming noncombatants is permissible only if the harms are proportional to the goals the attack is intended and reasonably expected to achieve.
\item	\emph{Necessity:} Collaterally harming noncombatants is permissible only if, in the pursuit of one's military objectives, the least harmful means feasible are chosen.
        \end{itemize}

You are told that you can safely assume drone operators have weighed these principles before initiating a strike. However, circumstances may change after a strike has been initiated in ways that the operators cannot anticipate, and due to poor connectivity, they may not be able to override the command. Your function must decide, based on current circumstances, whether to continue with the strike or abort. While autonomous systems are unable to make the same kind of judgments that human beings make, you may be able to program them to respond to changing circumstances in a manner that abides by the principles above.


Based on these considerations, answer the following questions (which you will submit in a PDF via Gradescope).

\begin{enumerate}
\item[(1)] 	Who are the stakeholders in this kind of scenario (i.e. who are the individuals whose rights and interests are likely to be affected by your design decisions)?

\item[(2)]	Think about what your function should do in the following specific situation:

  \begin{quote}\it
    The drone operator has initiated a strike to prevent an attack on civilian lives.  Network connectivity between the drone and the operator dropped before the strike was carried out. In the intervening time, the drone detects the presence of other human beings, who would likely be harmed by the strike. The drone cannot detect any signs that decisively mark these individuals as combatants, or non-combatants.
  \end{quote}
\begin{enumerate}
\item[(a)] 	In this specific situation, should the drone abort the strike? Why?
\item[(b)]	For this specific situation, how does your decision (of whether to abort or not) impact the stakeholder's rights and interests? How does it instantiate the ethical principles above? How does it balance them?
\end{enumerate}

\item[(3)]	Suppose that we have a similar situation as in (2), but now that the network connection has not dropped but has higher latency that normal, say, a round trip time of 1 second.
  \begin{enumerate}
  \item[(a)] 	In this specific situation, should the drone abort the strike? Why?
\item[(b)]	For this specific situation, how does your decision (of whether to abort or not) impact the stakeholder's rights and interests? How does it instantiate the ethical principles above? How does it balance them?
  \end{enumerate}

\end{enumerate}
Think of more general situations, i.e., the algorithm for your function. You may want to think about how you would design/implement your function, whether there are additional inputs that you would like your function to receive, and how your intended design of the function reflects the ethical principles above.


\end{question}


\end{document}
